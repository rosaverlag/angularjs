(function () {
'use strict';

angular.module('Assthree', [])
.controller('NarrowItDownController', Narrowdown)
.service('MenuCategoriesService', MenuCategoriesService)
.directive('foundItems', FoundItemsDirective);


function FoundItemsDirective(){
  var ddo = {
    templateUrl: 'foundItems.html',
    scope:{
      foundname: '<',
      onRemove: '&'
    },
    controller: Narrowdown,
    controllerAs: 'nrdown',
    bindToController: true
  };

  return ddo;
}

Narrowdown.$inject = ['MenuCategoriesService'];
function Narrowdown(MenuCategoriesService){
  var nrdown = this;
  nrdown.searchname = function(searchTerm){
    //console.log(searchTerm);
    MenuCategoriesService.getMatchedMenuItems(searchTerm);
  };

  nrdown.foundname = MenuCategoriesService.getfoundname();
  console.log("this is foundname: ", nrdown.foundname);

  nrdown.removeItem = function (itemIndex) {
    MenuCategoriesService.removeItem(itemIndex);
  };
}

MenuCategoriesService.$inject = ["$http"]
function MenuCategoriesService($http){
  var service = this;
  console.log("start");
  var foundname = [];
  service.getMatchedMenuItems = function(searchTerm){
    return $http({
      method : 'GET',
      url: ("https://davids-restaurant.herokuapp.com/menu_items.json")
    }).then(function(result){
        angular.forEach(result.data.menu_items, function(value, key){
          var name = value.name;
          //console.log(searchTerm);
          if (name.toLowerCase().indexOf(searchTerm) !== -1){
            foundname.push(name);
          };
        });
        console.log("end");
      });
  };

  service.removeItem = function(itemIndex){
    foundname.splice(itemIndex, 1);
    //console.log(foundname);
    return foundname;
  };

  service.getfoundname = function () {
    return foundname;
  };
}

})();
