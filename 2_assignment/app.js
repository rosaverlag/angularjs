(function () {
'use strict';

angular.module('ShoppinglistApp', [])
.controller('tobuylist', tobuylist)
.controller ('boughtlist', boughtlist)
.service('shoppinglistservice', shoppinglistservice);

tobuylist.$inject = ['shoppinglistservice'];
function tobuylist(shoppinglistservice){
      var tobuy = this;
      tobuy.shoppinglist = [
      {
        name: "Milk",
        quantity: "2"
      },
      {
        name: "Donuts",
        quantity: "200"
      },
      {
        name: "Cookies",
        quantity: "300"
      },
      {
        name: "Chocolate",
        quantity: "5"
      },
      {
        name: "Banane",
        quantity: "5"
      }
    ];

    tobuy.addandremoveitem = function (quantity, item, index){
       shoppinglistservice.additem(quantity, item);
       tobuy.shoppinglist.splice(index, 1);
       //console.log("shoppinglist", tobuy.shoppinglist);

       if(tobuy.shoppinglist.length==0){
           tobuy.message = "Everything is bought";
       };

    };

}


boughtlist.$inject = ['shoppinglistservice'];
function boughtlist(shoppinglistservice){
    var bought = this;
    bought.items = shoppinglistservice.getitems();
    console.log(bought.items);
     //if(bought.items.length==0){
        bought.message = "Nothing bought yet!";
   //};

     // try{
     //   shoppinglistservice.getitems();
     // }catch (error){
     //   bought.message = error.message;
     // };
}



function shoppinglistservice(){
  var service = this;
  var items = [];

    service.additem = function (itemname, itemquantity){
      var item = {
        name : itemname,
        quantity : itemquantity
      };
      items.push(item);
    };

    service.getitems = function(){
      // if (items.length==0){
      //    throw new Error("NOOOOOOOO");
      //  };
       return items;
   };




 }

})();
