(function() {
  'use strict';

  angular.module('MenuApp')
  .controller('itemsController', itemsController);

  // Spiegazione: la variabile categories viene presa da routes.js dove viene risolta la promise del MenuDataService.
  // Quindi le routes vengono usate per trasferire i dati presi nel service al controller e da question alla view.

  categoriesController.$inject = ['$stateParams', 'MenuDataService', 'items'];

  function itemsController($stateParams, MenuDataService, items){
    var itemsCtrl = this;
    itemsCtrl.items = items;
    console.log(itemsCtrl.items);
    itemsCtrl.categoryName = $stateParams.categoryName; 

  }
})();
