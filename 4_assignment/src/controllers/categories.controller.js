(function() {
  'use strict';

  angular.module('MenuApp')
  .controller('categoriesController', categoriesController);

  // Spiegazione: la variabile categories viene presa da routes.js dove viene risolta la promise del MenuDataService.
  // Quindi le routes vengono usate per trasferire i dati presi nel service al controller e da question alla view.

  categoriesController.$inject = ['MenuDataService', 'categories'];

  function categoriesController(MenuDataService, categories){
    var categoriesCtrl = this;
    categoriesCtrl.categories = categories;
    //console.log(categoriesCtrl.categories);

  }
})();
